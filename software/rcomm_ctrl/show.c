#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/sysinfo.h>
#include <time.h>
#include <poll.h>

int main(int argc, char *argv[]){
	int fd, t, refresh;
	int cts, ri, dsr, dcd;
	fd = open(argv[1], O_RDWR);
	if(fd < 0){
		printf("open\n");
		return(1);
	}
	cts = ri = dsr = dcd = -1;
	while(1){
		if(ioctl(fd, TIOCMGET, &t) < 0){
			fprintf(stderr, "ioctl\n");
			return(1);
		}
		refresh = 0;
		if(t & TIOCM_CTS){
			if(cts != 1){
				cts = 1;
				refresh = 1;
			}
		}else{
			if(cts != 0){
				cts = 0;
				refresh = 1;
			}
		}
		if(t & TIOCM_RI){
			if(ri != 1){
				ri = 1;
				refresh = 1;
			}
		}else{
			if(ri != 0){
				ri = 0;
				refresh = 1;
			}
		}
		if(t & TIOCM_DSR){
			if(dsr != 1){
				dsr = 1;
				refresh = 1;
			}
		}else{
			if(dsr != 0){
				dsr = 0;
				refresh = 1;
			}
		}
		if(t & TIOCM_CD){
			if(dcd != 1){
				dcd = 1;
				refresh = 1;
			}
		}else{
			if(dcd != 0){
				dcd = 0;
				refresh = 1;
			}
		}
		if(refresh){
			printf("CTS: %d, RI: %d, DSR: %d, DCD: %d\n", cts, ri, dsr, dcd);
		}
		usleep(10000);
	}
}

