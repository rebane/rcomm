#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/sysinfo.h>
#include <time.h>
#include <poll.h>

int uart_set_dtr(int fd, int on);
int uart_set_rts(int fd, int on);

int main(int argc, char *argv[]){
	int fd;
	struct termios tio;
	fd = open(argv[1], O_RDWR);
	if(fd < 0){
		printf("open\n");
		return(1);
	}
	ioctl(fd, TCGETS, &tio);
	tio.c_cflag &= ~HUPCL;
	//printf("cflag: 0x%08X\n", tio.c_cflag);
	//printf("iflag: 0x%08X\n", tio.c_iflag);
	//printf("oflag: 0x%08X\n", tio.c_oflag);
	ioctl(fd, TCSETS, &tio);

	if(atoi(argv[2])){
		uart_set_rts(fd, 0);
	}else{
		uart_set_rts(fd, 1);
	}
	if(atoi(argv[3])){
		uart_set_dtr(fd, 0);
	}else{
		uart_set_dtr(fd, 1);
	}
}

int uart_set_dtr(int fd, int on){
	int t;
	if(ioctl(fd, TIOCMGET, &t) < 0)return(-1);
	if(on){
		t |= TIOCM_DTR;
	}else{
		t &= ~TIOCM_DTR;
	}
	if(ioctl(fd, TIOCMSET, &t) < 0)return(-1);
	return(1);
}

int uart_set_rts(int fd, int on){
	int t;
	if(ioctl(fd, TIOCMGET, &t) < 0)return(-1);
	if(on){
		t |= TIOCM_RTS;
	}else{
		t &= ~TIOCM_RTS;
	}
	if(ioctl(fd, TIOCMSET, &t) < 0)return(-1);
	return(1);
}

