#ifndef _COLOR_H_
#define _COLOR_H_

#define COLOR_NONE    253
#define COLOR_DEFAULT 254
#define COLOR_NEXT    255

#define COLOR_BLACK   0
#define COLOR_RED     1
#define COLOR_GREEN   2
#define COLOR_YELLOW  3
#define COLOR_BLUE    4
#define COLOR_MAGENTA 5
#define COLOR_CYAN    6
#define COLOR_WHITE   7

int color_parse(const char *s);
int color_write(int color, const void *buf, int count);
int color_printf(int color, const void *fmt, ...);
void color_lf();

#endif

