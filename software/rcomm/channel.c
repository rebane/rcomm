#include "channel.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "uart.h"
#include "color.h"

void channel_reset(channel_t *channel){
	channel->fd = -1;
	channel->device = NULL;
	channel->helper = NULL;
	channel->helper_parameter = NULL;
	channel->baudrate = 115200;
	channel->bits = 8;
	channel->parity = UART_PARITY_NONE;
	channel->stop = 1;
	channel->flow = UART_FLOW_NONE;
	channel->color = COLOR_DEFAULT;
	channel->receive_conversion = 0;
	channel->hex_mode = 0;
	channel->line_mode = 0;
	channel->modem_rts = -1;
	channel->modem_dtr = -1;
	channel->rs485_flags = 0;
	channel->line_loc = 0;
	channel->major = 0;
	channel->minor = 0;
	channel->pause = 0;
	channel->paused = 0;
}

int channel_parse_baud(const char *baud, channel_t *channel){
	int i;
	for(i = 0; baud[i]; i++){
		if(!isdigit(baud[i]))return(-1);
	}
	channel->baudrate = strtol(baud, NULL, 10);
	return(1);
}

int channel_parse_mode(const char *mode, channel_t *channel){
	int bits, parity, stop;
	char s[2];
	if(strlen(mode) != 3)return(-1);
	s[1] = 0;
	s[0] = mode[0];
	bits = channel_parse_bits(s);
	if(bits < 0)return(-1);
	s[0] = mode[1];
	parity = channel_parse_parity(s);
	if(parity < 0)return(-1);
	s[0] = mode[2];
	stop = channel_parse_stop(s);
	if(stop < 0)return(-1);
	channel->bits = bits;
	channel->parity = parity;
	channel->stop = stop;
	return(1);
}

int channel_parse_bits(const char *bits){
	if(!strcmp(bits, "5"))return(5);
	if(!strcmp(bits, "6"))return(6);
	if(!strcmp(bits, "7"))return(7);
	if(!strcmp(bits, "8"))return(8);
	if(!strcmp(bits, "9"))return(9);
	return(-1);
}

int channel_parse_parity(const char *parity){
	if(!strcasecmp(parity, "n") || !strcasecmp(parity, "none"))return(UART_PARITY_NONE);
	if(!strcasecmp(parity, "e") || !strcasecmp(parity, "even"))return(UART_PARITY_EVEN);
	if(!strcasecmp(parity, "o") || !strcasecmp(parity, "odd"))return(UART_PARITY_ODD);
	if(!strcasecmp(parity, "m") || !strcasecmp(parity, "mark"))return(UART_PARITY_MARK);
	if(!strcasecmp(parity, "s") || !strcasecmp(parity, "space"))return(UART_PARITY_SPACE);
	return(-1);
}

int channel_parse_stop(const char *stop){
	if(!strcmp(stop, "1"))return(1);
	if(!strcmp(stop, "2"))return(2);
	return(-1);
}

int channel_parse_modem(const char *modem, channel_t *channel){
	int i;
	for(i = 0; modem[i]; i++){
		if(modem[i] == 'r'){
			if(channel->modem_rts != -1)return(-1);
			channel->modem_rts = 0;
		}else if(modem[i] == 'R'){
			if(channel->modem_rts != -1)return(-1);
			channel->modem_rts = 1;
		}else if(modem[i] == 'd'){
			if(channel->modem_dtr != -1)return(-1);
			channel->modem_dtr = 0;
		}else if(modem[i] == 'D'){
			if(channel->modem_dtr != -1)return(-1);
			channel->modem_dtr = 1;
		}else{
			return(-1);
		}
	}
	return(1);
}

void channel_debug(int c, channel_t *channel){
	printf("============\n");
	printf("CHANNEL: %d\n", c);
	printf("DEVICE: %s\n", channel[c].device);
	printf("BAUD: %lu\n", channel[c].baudrate);
	printf("BITS: %d\n", channel[c].bits);
	printf("PARITY: %d\n", channel[c].parity);
	printf("STOP: %d\n", channel[c].stop);
	printf("FLOW: %d\n", channel[c].flow);
	printf("COLOR: %d\n", channel[c].color);
}

