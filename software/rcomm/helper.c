#include "helper.h"
#include "channel.h"

void helper_init(channel_t *c, int init){
	if(!c->use_helper)return;
}

int helper_ready(channel_t *c){
	if(!c->use_helper)return(1);
	return(1);
}

void helper_poll(channel_t *c){
	if(!c->use_helper)return;
}

