#ifndef _HELPER_H_
#define _HELPER_H_

#include "channel.h"

void helper_init(channel_t *c, int init);
int helper_ready(channel_t *c);
void helper_poll(channel_t *c);

#endif

