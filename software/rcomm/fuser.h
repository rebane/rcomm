#ifndef _FUSER_H_
#define _FUSER_H_

#include <sys/types.h>

int fuser(const char *test, char *file, size_t size, pid_t *pids, int pids_count);

#endif

