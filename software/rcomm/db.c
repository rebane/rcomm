#include "db.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#ifdef USE_DBUS

#include <dbus/dbus.h>

#define DB_MAX_WATCHES 16

static DBusError db_err;
static DBusConnection *db_conn;
static DBusWatch *db_watch[DB_MAX_WATCHES];

static dbus_bool_t db_add_watch(DBusWatch *w, void *d);
static void db_del_watch(DBusWatch *w, void *d);
static DBusHandlerResult db_filter(DBusConnection *c, DBusMessage *m, void *data);

void db_init(){
	int i;
	for(i = 0; i < DB_MAX_WATCHES; i++){
		db_watch[i] = NULL;
	}
	dbus_error_init(&db_err);
	db_conn = dbus_bus_get(DBUS_BUS_SESSION, &db_err);
	if(dbus_error_is_set(&db_err)){
		dbus_error_free(&db_err);
	}
	if(db_conn == NULL)return;
	dbus_connection_set_watch_functions(db_conn, db_add_watch, db_del_watch, NULL, NULL, dbus_free);
	dbus_connection_add_filter(db_conn, db_filter, NULL, NULL);
}

void db_register(int major, int minor){
	char buffer[512];
	if(db_conn == NULL)return;
	snprintf(buffer, 511, "com.rcomm.device_%d_%d", major, minor);
	buffer[511] = 0;
	dbus_bus_request_name(db_conn, buffer, DBUS_NAME_FLAG_REPLACE_EXISTING, &db_err);
	if(dbus_error_is_set(&db_err)){
		dbus_error_free(&db_err);
	}
}

void db_unregister(int major, int minor){
	char buffer[512];
	if(db_conn == NULL)return;
	snprintf(buffer, 511, "com.rcomm.device_%d_%d", major, minor);
	buffer[511] = 0;
	dbus_bus_release_name(db_conn, buffer, &db_err);
	if(dbus_error_is_set(&db_err)){
		dbus_error_free(&db_err);
	}
}

int db_fd_set(int fd_max, fd_set *rfds, fd_set *wfds){
	int i, fd, flags;
	if(db_conn == NULL)return(fd_max);
	for(i = 0; i < DB_MAX_WATCHES; i++){
		if(db_watch[i] == NULL)continue;
		if(!dbus_watch_get_enabled(db_watch[i]))continue;
		fd = dbus_watch_get_unix_fd(db_watch[i]);
		flags = dbus_watch_get_flags(db_watch[i]);
		if(flags & DBUS_WATCH_READABLE){
			FD_SET(fd, rfds);
		}
		if(flags & DBUS_WATCH_WRITABLE){
			FD_SET(fd, wfds);
		}
		if(fd > fd_max)fd_max = fd;
	}
	return(fd_max);
}

void db_fd_handle(fd_set *rfds, fd_set *wfds){
	int i, fd, flags, handle;
	if(db_conn == NULL)return;
	for(i = 0; i < DB_MAX_WATCHES; i++){
		if(db_watch[i] == NULL)continue;
		if(!dbus_watch_get_enabled(db_watch[i]))continue;
		handle = 0;
		fd = dbus_watch_get_unix_fd(db_watch[i]);
		flags = dbus_watch_get_flags(db_watch[i]);
		if(flags & DBUS_WATCH_READABLE){
			if(FD_ISSET(fd, rfds)){
				handle |= DBUS_WATCH_READABLE;
			}
		}
		if(flags & DBUS_WATCH_WRITABLE){
			if(FD_ISSET(fd, wfds)){
				handle |= DBUS_WATCH_WRITABLE;
			}
		}
		if(handle){
			// https://lists.freedesktop.org/archives/dbus/2007-October/008859.html
			while(!dbus_watch_handle(db_watch[i], handle)){
				usleep(100000);
			}
			dbus_connection_ref(db_conn);
			while(dbus_connection_dispatch(db_conn) == DBUS_DISPATCH_DATA_REMAINS);
			dbus_connection_unref(db_conn);
		}
	}
}

static dbus_bool_t db_add_watch(DBusWatch *w, void *d){
	int i;
	for(i = 0; i < DB_MAX_WATCHES; i++){
		if(db_watch[i] == w)return(1);
	}
	for(i = 0; i < DB_MAX_WATCHES; i++){
		if(db_watch[i] == NULL){
			db_watch[i] = w;
			break;
		}
	}
	return(1);
}

static void db_del_watch(DBusWatch *w, void *d){
	int i;
	for(i = 0; i < DB_MAX_WATCHES; i++){
		if(db_watch[i] == w){
			db_watch[i] = NULL;
		}
	}
}

static DBusHandlerResult db_filter(DBusConnection *c, DBusMessage *m, void *data){
	int i, major, minor;
	DBusMessageIter args;
	dbus_bool_t b;
	dbus_uint32_t u32;
	dbus_int32_t i32;
	dbus_uint64_t u64;
	dbus_int64_t i64;
	char *s, *p[5], buffer[64];
	p[0] = p[1] = p[2] = p[3] = p[4] = NULL;
	if((sscanf(dbus_message_get_destination(m), "com.rcomm.device_%d_%d", &major, &minor) != 2) || strcmp(dbus_message_get_interface(m), "com.rcomm.device")){
		if(dbus_message_get_type(m) == DBUS_MESSAGE_TYPE_METHOD_CALL){
			db_reply(m, -1, NULL);
		}
		return(DBUS_HANDLER_RESULT_HANDLED);
	}
	if(dbus_message_iter_init(m, &args)){
		for(i = 0; i < 5; i++){
			if(dbus_message_iter_get_arg_type(&args) == DBUS_TYPE_BOOLEAN){
				dbus_message_iter_get_basic(&args, &b);
				if(b){
					p[i] = strdup("1");
				}else{
					p[i] = strdup("0");
				}
			}else if(dbus_message_iter_get_arg_type(&args) == DBUS_TYPE_UINT32){
				dbus_message_iter_get_basic(&args, &u32);
				snprintf(buffer, 63, "%lu", (unsigned long int)u32);
				buffer[63] = 0;
				p[i] = strdup(buffer);
			}else if(dbus_message_iter_get_arg_type(&args) == DBUS_TYPE_INT32){
				dbus_message_iter_get_basic(&args, &i32);
				snprintf(buffer, 63, "%ld", (long int)i32);
				buffer[63] = 0;
				p[i] = strdup(buffer);
			}else if(dbus_message_iter_get_arg_type(&args) == DBUS_TYPE_UINT64){
				dbus_message_iter_get_basic(&args, &u64);
				snprintf(buffer, 63, "%llu", (unsigned long long int)u64);
				buffer[63] = 0;
				p[i] = strdup(buffer);
			}else if(dbus_message_iter_get_arg_type(&args) == DBUS_TYPE_INT64){
				dbus_message_iter_get_basic(&args, &i64);
				snprintf(buffer, 63, "%lld", (long long int)i64);
				buffer[63] = 0;
				p[i] = strdup(buffer);
			}else if(dbus_message_iter_get_arg_type(&args) == DBUS_TYPE_STRING){
				dbus_message_iter_get_basic(&args, &s);
				p[i] = strdup(s);
			}else{
				p[i] = strdup("unknown");
			}
			if(!dbus_message_iter_next(&args))break;
		};
	}
	db_callback((void *)m, major, minor, (char *)dbus_message_get_member(m), p[0], p[1], p[2], p[3], p[4]);
	free(p[0]);
	free(p[1]);
	free(p[2]);
	free(p[3]);
	free(p[4]);
	return(DBUS_HANDLER_RESULT_HANDLED);
}

void db_reply(void *msg, int ret, char *p){
	DBusMessage *reply;
	DBusMessageIter args;
	dbus_int32_t i32;
	reply = dbus_message_new_method_return((DBusMessage *)msg);
   	dbus_message_iter_init_append(reply, &args);
	i32 = ret;
	dbus_message_iter_append_basic(&args, DBUS_TYPE_INT32, &i32);
	if(p != NULL){
		dbus_message_iter_append_basic(&args, DBUS_TYPE_STRING, &p);
	}
	dbus_connection_send(db_conn, reply, NULL);
	dbus_connection_flush(db_conn);
	dbus_message_unref(reply);
}

#else
void db_init(){
}

void db_register(int major, int minor){
}

void db_unregister(int major, int minor){
}

int db_fd_set(int fd_max, fd_set *rfds, fd_set *wfds){
	return(fd_max);
}

void db_fd_handle(fd_set *rfds, fd_set *wfds){
}

void db_reply(void *msg, int ret, char *p){
}

#endif
