#include "color.h"
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <stdarg.h>

static int color_next = COLOR_BLACK;
static volatile int color_lf_needed = 0;

int color_parse(const char *s){
	if(!strcasecmp(s, "none"))return(COLOR_NONE);
	if(!strcasecmp(s, "default"))return(COLOR_DEFAULT);
	if(!strcasecmp(s, "next")){
		color_next++;
		if(color_next > COLOR_WHITE)color_next = COLOR_RED;
		return(color_next);
	}
	if(!strcasecmp(s, "black")){
		color_next = COLOR_BLACK;
		return(COLOR_BLACK);
	}
	if(!strcasecmp(s, "red")){
		color_next = COLOR_RED;
		return(COLOR_RED);
	}
	if(!strcasecmp(s, "green")){
		color_next = COLOR_GREEN;
		return(COLOR_GREEN);
	}
	if(!strcasecmp(s, "yellow")){
		color_next = COLOR_YELLOW;
		return(COLOR_YELLOW);
	}
	if(!strcasecmp(s, "blue")){
		color_next = COLOR_BLUE;
		return(COLOR_BLUE);
	}
	if(!strcasecmp(s, "magenta")){
		color_next = COLOR_MAGENTA;
		return(COLOR_MAGENTA);
	}
	if(!strcasecmp(s, "cyan")){
		color_next = COLOR_CYAN;
		return(COLOR_CYAN);
	}
	if(!strcasecmp(s, "white")){
		color_next = COLOR_WHITE;
		return(COLOR_WHITE);
	}
	return(-1);
}

int color_write(int color, const void *buf, int count){
	if(color == COLOR_NONE){
		return(count);
	}else if(color == COLOR_DEFAULT){
	}else if(color == COLOR_BLACK){
		if(write(STDOUT_FILENO, "\x1b[1;30m", 7) < 7)return(-1);
	}else if(color == COLOR_RED){
		if(write(STDOUT_FILENO, "\x1b[0;31m", 7) < 7)return(-1);
	}else if(color == COLOR_GREEN){
		if(write(STDOUT_FILENO, "\x1b[0;32m", 7) < 7)return(-1);
	}else if(color == COLOR_YELLOW){
		if(write(STDOUT_FILENO, "\x1b[1;33m", 7) < 7)return(-1);
	}else if(color == COLOR_BLUE){
		if(write(STDOUT_FILENO, "\x1b[1;34m", 7) < 7)return(-1);
	}else if(color == COLOR_MAGENTA){
		if(write(STDOUT_FILENO, "\x1b[0;35m", 7) < 7)return(-1);
	}else if(color == COLOR_CYAN){
		if(write(STDOUT_FILENO, "\x1b[0;36m", 7) < 7)return(-1);
	}else if(color == COLOR_WHITE){
		if(write(STDOUT_FILENO, "\x1b[1;37m", 7) < 7)return(-1);
	}
	if(write(STDOUT_FILENO, buf, count) < count)return(-1);
	if(count && (((char *)buf)[count - 1] == '\n')){
		color_lf_needed = 0;
	}else{
		color_lf_needed = 1;
	}
	if(color != COLOR_DEFAULT){
		if(write(STDOUT_FILENO, "\x1b[0m", 4) < 4)return(-1);
	}
	return(count);
}

int color_printf(int color, const void *fmt, ...){
	va_list ap;
	va_start(ap, fmt);
	write(STDOUT_FILENO, "\x1b[0;35m", 7);
	vprintf(fmt, ap);
	fflush(stdout);
	va_end(ap);
	write(STDOUT_FILENO, "\x1b[0m", 4);
	color_lf_needed = 0;
	return(0);
}

void color_lf(){
	if(color_lf_needed){
		printf("\n");
		color_lf_needed = 0;
	}
}

