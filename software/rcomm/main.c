#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <poll.h>
#include <string.h>
#include <signal.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <errno.h>
#include "uart.h"
#include "channel.h"
#include "color.h"
#include "fuser.h"
#include "helper.h"
#include "db.h"
#include "method.h"

// https://linoxide.com/how-tos/d-bus-ipc-mechanism-linux/
// https://stackoverflow.com/questions/17980725/locking-linux-serial-port
// https://www.kernel.org/doc/Documentation/serial/serial-rs485.txt

#define CHANNEL_MAX 8
#define BUF_SIZE    16384
#define PID_COUNT   16

static channel_t channel[CHANNEL_MAX];
static int channel_max = 0, echo_color = COLOR_NONE;
volatile int ctrlc = 0, ctrlz = 0, ctrlbackslash = 0;
static struct termios termio;
static int hex_count;

void output_write(int c, char *buffer, int len);

void handler_int(int signal){
	if(ctrlc && (signal == SIGINT)){
		write(channel[0].fd, "\x03", 1);
	}else{
		if(signal == SIGINT){
			printf("\n");
		}else{
			color_lf();
		}
		tcsetattr(STDIN_FILENO, TCSANOW, &termio);
		fflush(stdout);
		fflush(stderr);
		exit(1);
	}
}

void handler_tstp(int signal){
	write(channel[0].fd, "\x1A", 1);
}

void handler_quit(int signal){
	write(channel[0].fd, "\x1C", 1);
}

void handler_pause_resume(int signal){
	int c;
	if(signal == SIGUSR1){
		for(c = 0; c < channel_max; c++){
			channel[c].pause = 1;
		}
	}else if(signal == SIGUSR2){
		for(c = 0; c < channel_max; c++){
			channel[c].pause = 0;
		}
	}
}

void help(){
	printf("global parameters:\n");
	printf("  D - debug mode\n");
	printf("  F - disable fuser test\n");
	printf("  M - method call (device, method, params ...)\n");
	printf("\n");
	printf("send parameters:\n");
	printf("  e - local echo color (enables local echo)\n"); // none, default, next, red, blue ...
	printf("  C - enable ctrl-c (to exit, press ctrl-x)\n");
	printf("  Z - enable ctrl-z\n");
	printf("  Q - enable ctrl-\\\n");
	printf("  S - send conversion (rntrn)\n");
	printf("  R - read only (do not send)\n"); // TODO:
	printf("  W - write only (do not read from channel 0)\n"); // TODO:
	printf("\n");
	printf("  c - next channel color (switches to next channel)\n");
	printf("channel parameters:\n");
	printf("  d - device\n");
	printf("  b - baudrate\n");
	printf("  n - bits\n");
	printf("  p - parity\n");
	printf("  s - stop bits\n");
	printf("  f - rts/cts flow control\n");
	printf("  r - receive conversion (rntrn)\n");
	printf("  x - hex mode\n");
	printf("  l - line mode\n");
	printf("  H - helper\n");
	printf("  P - helper parameter\n");
	printf("  m - modem lines (r - RTS TTL LOW, R - RTS TTL HIGH, d - DTR TTL LOW, D - DTR TTL HIGH)\n");
	printf("  4 - RS485\n");
	printf("  8 - RS485 with RTS on send\n");
}

int main(int argc, char *argv[]){
	int c, i, j, k, debug, fuser_test, send_conversion, ttl_high, ttl_low, method;
	static struct termios t;
	struct timeval tv;
	char *b, buffer[BUF_SIZE], buffer2[BUF_SIZE * 2];
	fd_set rfds, wfds;
	pid_t pids[PID_COUNT];
	int pid_count;
	char *method_method, *method_param[5];

	if (argc < 2) {
		help();
		return(0);
	}

	echo_color = COLOR_NONE;
	i = 0;
	debug = 0;
	fuser_test = 1;
	send_conversion = 0;
	hex_count = 0;
	method = 0;
	method_method = NULL;
	method_param[0] = method_param[1] = method_param[2] = method_param[3] = method_param[4] = NULL;
	channel_reset(&channel[0]);

	opterr = 0;
	while((c = getopt(argc, argv, "-hDFMe:CZQSRWc:d:b:n:p:s:frxlH:P:m:48")) != -1){
		if(c == 'h'){
			help();
			return(0);
		}else if(c == 'D'){
			debug = 1;
		}else if(c == 'F'){
			fuser_test = 0;
		}else if(c == 'M'){
			if(!method)method = 1;
		}else if(c == 'e'){
			echo_color = color_parse(optarg);
			if(echo_color < 0){
				fprintf(stderr, "cannot parse color: %s\n", optarg);
				return(-1);
			}
		}else if(c == 'C'){
			ctrlc = 1;
		}else if(c == 'Z'){
			ctrlz = 1;
		}else if(c == 'Q'){
			ctrlbackslash = 1;
		}else if(c == 'S'){
			send_conversion = 1;
		}else if(c == 'R'){
		}else if(c == 'W'){
		}else if(c == 'c'){
			if(channel_max || i){
				channel_max++;
				if(channel_max >= CHANNEL_MAX){
					fprintf(stderr, "max channels reached\n");
					return(-1);
				}
				channel_reset(&channel[channel_max]);
			}
			channel[channel_max].color = color_parse(optarg);
			if(channel[channel_max].color < 0){
				fprintf(stderr, "cannot parse color: %s\n", optarg);
				return(-1);
			}
			i = 1;
		}else if(c == 'd'){
			channel[channel_max].device = optarg;
			i = 1;
		}else if(c == 'b'){
			if(channel_parse_baud(optarg, &channel[channel_max]) < 0){
				fprintf(stderr, "cannot parse baudrate: %s\n", optarg);
				return(-1);
			}
			i = 1;
		}else if(c == 'n'){
			channel[channel_max].bits = channel_parse_bits(optarg);
			if(channel[channel_max].bits < 0){
				fprintf(stderr, "cannot parse bits: %s\n", optarg);
				return(-1);
			}
			i = 1;
		}else if(c == 'p'){
			channel[channel_max].parity = channel_parse_parity(optarg);
			if(channel[channel_max].parity < 0){
				fprintf(stderr, "cannot parse parity: %s\n", optarg);
				return(-1);
			}
			i = 1;
		}else if(c == 's'){
			channel[channel_max].stop = channel_parse_stop(optarg);
			if(channel[channel_max].stop < 0){
				fprintf(stderr, "cannot parse stop bits: %s\n", optarg);
				return(-1);
			}
			i = 1;
		}else if(c == 'f'){
			channel[channel_max].flow = 1;
			i = 1;
		}else if(c == 'r'){
			channel[channel_max].receive_conversion = 1;
			i = 1;
		}else if(c == 'x'){
			channel[channel_max].hex_mode = 1;
			i = 1;
		}else if(c == 'l'){
			channel[channel_max].line_mode = 1;
			i = 1;
		}else if(c == 'H'){
			channel[channel_max].helper = optarg;
			i = 1;
		}else if(c == 'P'){
			channel[channel_max].helper_parameter = optarg;
			i = 1;
		}else if(c == 'm'){
			if(channel_parse_modem(optarg, &channel[channel_max]) < 0){
				fprintf(stderr, "cannot parse modem lines: %s\n", optarg);
				return(-1);
			}
			i = 1;
		}else if(c == '4'){
			channel[channel_max].rs485_flags |= UART_RS485_ENABLED | UART_RS485_RTS_ON_SEND;
			i = 1;
		}else if(c == '8'){
			channel[channel_max].rs485_flags |= UART_RS485_ENABLED;
			i = 1;
		}else if(c == '\1'){
			if(method){
				if(method == 1){
					method_method = optarg;
				}else if(method < 7){
					method_param[method - 2] = optarg;
				}
				if(method < 7)method++;
			}else{
				i = 1;
				if(channel_parse_baud(optarg, &channel[0]) > 0)continue;
				if(channel_parse_mode(optarg, &channel[0]) > 0)continue;
				channel[0].device = optarg;
			}
		}else{
			fprintf(stderr, "invalid command line\n");
			return(1);
		}
	}
	channel_max++;

	if(method){
		return(method_invoke(channel[0].device, channel[0].helper, channel[0].helper_parameter, method_method, method_param[0], method_param[1], method_param[2], method_param[3], method_param[4]));
	}
	db_init();

	for(c = 0; c < channel_max; c++){
		if(channel[c].device == NULL){
			if(channel[c].helper == NULL){
				channel[c].helper = strdup(getenv("RCOMM_HELPER"));
			}
			if(channel[c].helper == NULL){
				fprintf(stderr, "missing device name / helper not set\n");
				return(-1);
			}
			channel[c].use_helper = 1;
		}
	}

	if(debug){
		for(c = 0; c < channel_max; c++){
			channel_debug(c, &channel[c]);
		}
	}

	for(c = 0; c < channel_max; c++){
		helper_init(&channel[c], 1);
		while(!helper_ready(&channel[c]))helper_poll(&channel[c]);
		if(fuser_test){
			pid_count = fuser(channel[c].device, buffer, BUF_SIZE, pids, PID_COUNT);
			if(pid_count){
				fprintf(stderr, "device is used: %s (%s)\n", channel[c].device, buffer);
				fprintf(stderr, "pid(s):");
				for(i = 0; i < pid_count; i++){
					fprintf(stderr, " %d", (int)pids[i]);
				}
				fprintf(stderr, "\n");
				return(-1);
			}
		}
		channel[c].fd = uart_open(channel[c].device);
		if(channel[c].fd < 0){
			fprintf(stderr, "cannot open device: %s: %s\n", channel[c].device, strerror(errno));
			return(-1);
		}
		channel[c].major = uart_major(channel[c].fd);
		channel[c].minor = uart_minor(channel[c].fd);
		db_register(channel[c].major, channel[c].minor);
		uart_flush(channel[c].fd);
		uart_setup(channel[c].fd, channel[c].baudrate, channel[c].bits, channel[c].parity, channel[c].stop, channel[c].flow);
		uart_setup_rs485(channel[c].fd, channel[c].rs485_flags, channel[c].rs485_delay_rts_before_send, channel[c].rs485_delay_rts_after_send);
		uart_flush(channel[c].fd);
		ttl_high = ttl_low = 0;
		if(channel[c].modem_rts == 1){
			ttl_high |= UART_PIN_RTS;
		}else if(channel[c].modem_rts == 0){
			ttl_low |= UART_PIN_RTS;
		}
		if(channel[c].modem_dtr == 1){
			ttl_high |= UART_PIN_DTR;
		}else if(channel[c].modem_dtr == 0){
			ttl_low |= UART_PIN_DTR;
		}
		uart_pins(channel[c].fd, ttl_high, ttl_low);
	}

	tcgetattr(STDIN_FILENO, &termio);
	t = termio;
	t.c_lflag &= ~(ICANON | ECHO);
	signal(SIGHUP, SIG_IGN);
	signal(SIGPIPE, SIG_IGN);
	signal(SIGUSR1, handler_pause_resume);
	signal(SIGUSR2, handler_pause_resume);
	signal(SIGINT, handler_int);
	signal(SIGTERM, handler_int);
	if(ctrlz)signal(SIGTSTP, handler_tstp);
	if(ctrlbackslash)signal(SIGQUIT, handler_quit);
	tcsetattr(STDIN_FILENO, TCSANOW, &t);

	while(1){
		for(c = 0; c < channel_max; c++){
			if(channel[c].pause == channel[c].paused)continue;
			channel[c].paused = channel[c].pause;
			color_lf();
			if(channel[c].paused){
				color_printf(COLOR_MAGENTA, "CHANNEL PAUSED: %d\n", c);
			}else{
				color_printf(COLOR_MAGENTA, "CHANNEL RESUMED: %d\n", c);
			}
		}
		FD_ZERO(&rfds);
		FD_ZERO(&wfds);
		FD_SET(STDIN_FILENO, &rfds);
		i = STDIN_FILENO;
		for(c = 0; c < channel_max; c++){
			if(channel[c].paused)continue;
			if(channel[c].fd < 0){
				channel[c].fd = uart_open(channel[c].device);
				if(channel[c].fd >= 0){
					channel[c].major = uart_major(channel[c].fd);
					channel[c].minor = uart_minor(channel[c].fd);
					db_register(channel[c].major, channel[c].minor);
					uart_flush(channel[c].fd);
					uart_setup(channel[c].fd, channel[c].baudrate, channel[c].bits, channel[c].parity, channel[c].stop, channel[c].flow);
					uart_setup_rs485(channel[c].fd, channel[c].rs485_flags, channel[c].rs485_delay_rts_before_send, channel[c].rs485_delay_rts_after_send);
					uart_flush(channel[c].fd);
					ttl_high = ttl_low = 0;
					if(channel[c].modem_rts == 1){
						ttl_high |= UART_PIN_RTS;
					}else if(channel[c].modem_rts == 0){
						ttl_low |= UART_PIN_RTS;
					}
					if(channel[c].modem_dtr == 1){
						ttl_high |= UART_PIN_DTR;
					}else if(channel[c].modem_dtr == 0){
						ttl_low |= UART_PIN_DTR;
					}
					uart_pins(channel[c].fd, ttl_high, ttl_low);
					color_lf();
					color_printf(COLOR_MAGENTA, "CHANNEL REOPENED: %d\n", c);
				}
			}
			if(channel[c].fd >= 0){
				FD_SET(channel[c].fd, &rfds);
				if(i < channel[c].fd)i = channel[c].fd;
			}
		}
		tv.tv_sec = 0;
		tv.tv_usec = 10000;
		i = db_fd_set(i, &rfds, &wfds);
		i = select(i + 1, &rfds, &wfds, NULL, &tv);
		if(i < 0){
			if(errno == EINTR)continue;
			fprintf(stderr, "select error\n");
			handler_int(0);
		}else if(i > 0){
			if(FD_ISSET(STDIN_FILENO, &rfds)){
				i = read(STDIN_FILENO, buffer, BUF_SIZE);
				if(i < 1){
					tcsetattr(STDIN_FILENO, TCSANOW, &termio);
					return(0);
				}
				j = i;
				b = buffer;
				if(send_conversion){
					for(j = 0, k = 0; j < i; j++, k++){
						if(buffer[j] == '\n')buffer2[k++] = '\r';
						buffer2[k] = buffer[j];
					}
					j = i;
					i = k;
					b = buffer2;
				}
				if(ctrlc){
					for(j = 0; j < i; j++){
						if(b[j] == 0x18){
							color_write(echo_color, buffer, j);
							write(channel[0].fd, b, j);
							printf("\n");
							handler_int(0);
						}
					}
				}
				if(color_write(echo_color, buffer, j) < 0){
					fprintf(stderr, "write stdout\n");
					handler_int(0);
				}
				write(channel[0].fd, b, i);
			}
			for(c = 0; c < channel_max; c++){
				if(channel[c].paused)continue;
				if((channel[c].fd >= 0) && FD_ISSET(channel[c].fd, &rfds)){
					i = read(channel[c].fd, buffer, BUF_SIZE);
					if(i < 1){
						uart_close(channel[c].fd);
						db_unregister(channel[c].major, channel[c].minor);
						channel[c].fd = -1;
						color_lf();
						color_printf(COLOR_MAGENTA, "CHANNEL CLOSED: %d\n", c);
					}else{
						if(channel[c].line_mode){
							for(j = 0; j < i; j++){
								if((buffer[j] == '\r') || (buffer[j] == '\n')){
									if(channel[c].line_loc){
										output_write(c, channel[c].line_buffer, channel[c].line_loc);
										if(color_write(channel[c].color, "\n", 1) < 0){
											fprintf(stderr, "write stdout\n");
											handler_int(0);
										}
										channel[c].line_loc = 0;
										hex_count = 0;
									}
								}else{
									if(channel[c].line_loc < (CHANNEL_LINE_BUFFER_LEN - 1)){
										channel[c].line_buffer[channel[c].line_loc] = buffer[j];
										channel[c].line_loc++;
									}
								}
							}
						}else{
							output_write(c, buffer, i);
						}
					}
				}
			}
			db_fd_handle(&rfds, &wfds);
		}
	}
}

void output_write(int c, char *buffer, int len){
	int j;
	char hex_tmp[16];
	if(channel[c].hex_mode){
		for(j = 0; j < len; j++){
			snprintf(hex_tmp, 15, "%02X", (unsigned int)((unsigned char)buffer[j]));
			if(hex_count){
				if(color_write(channel[c].color, " ", 1) < 0){
					fprintf(stderr, "write stdout\n");
					handler_int(0);
				}
			}
			if(color_write(channel[c].color, hex_tmp, 2) < 0){
				fprintf(stderr, "write stdout\n");
				handler_int(0);
			}
			hex_count++;
			if(hex_count == 16){
				if(color_write(channel[c].color, "\n", 1) < 0){
					fprintf(stderr, "write stdout\n");
					handler_int(0);
				}
				hex_count = 0;
			}
		}
	}else{
		if(channel[c].receive_conversion){
			for(j = 0; j < len; j++){
				if(buffer[j] == '\r')buffer[j] = '\n';
			}
		}
		if(color_write(channel[c].color, buffer, len) < 0){
			fprintf(stderr, "write stdout\n");
			handler_int(0);
		}
	}
}

void db_callback(void *msg, int major, int minor, char *method, char *p1, char *p2, char *p3, char *p4, char *p5){
	int c, ttl_high, ttl_low;
/*	printf("MAJOR: %d, MINOR: %d, METHOD: %s, P1: %s, P2: %s, P3: %s, P4: %s, P5: %s\n",
		major, minor, method,
		p1, p2, p3, p4, p5
	);*/
	for(c = 0; c < channel_max; c++){
		if(channel[c].fd < 0)continue;
		if((channel[c].major == major) && (channel[c].minor == minor))break;
	}
	if(c == channel_max){
		db_reply(msg, -1, "device not found");
	}
	if(!strcmp(method, "pause")){
		channel[c].pause = 1;
		db_reply(msg, 0, NULL);
	}else if(!strcmp(method, "resume")){
		channel[c].pause = 0;
		db_reply(msg, 0, NULL);
	}else if(!strcmp(method, "flush")){
		if(channel[c].fd >= 0){
			uart_flush(channel[c].fd);
		}
		db_reply(msg, 0, NULL);
	}else if(!strcmp(method, "restore_format")){
		if(channel[c].fd >= 0){
			uart_flush(channel[c].fd);
			uart_setup(channel[c].fd, channel[c].baudrate, channel[c].bits, channel[c].parity, channel[c].stop, channel[c].flow);
			uart_setup_rs485(channel[c].fd, channel[c].rs485_flags, channel[c].rs485_delay_rts_before_send, channel[c].rs485_delay_rts_after_send);
			uart_flush(channel[c].fd);
			db_reply(msg, 0, NULL);
		}else{
			db_reply(msg, -1, NULL);
		}
	}else if(!strcmp(method, "restore_pins")){
		if(channel[c].fd >= 0){
			ttl_high = ttl_low = 0;
			if(channel[c].modem_rts == 1){
				ttl_high |= UART_PIN_RTS;
			}else if(channel[c].modem_rts == 0){
				ttl_low |= UART_PIN_RTS;
			}
			if(channel[c].modem_dtr == 1){
				ttl_high |= UART_PIN_DTR;
			}else if(channel[c].modem_dtr == 0){
				ttl_low |= UART_PIN_DTR;
			}
			uart_pins(channel[c].fd, ttl_high, ttl_low);
			db_reply(msg, 0, NULL);
		}else{
			db_reply(msg, -1, NULL);
		}
	}else if(!strcmp(method, "set_rts")){
		if(channel[c].fd >= 0){
			ttl_high = ttl_low = 0;
			if(p1 != NULL){
				if(atoi(p1)){
					ttl_high |= UART_PIN_RTS; 
				}else{
					ttl_low |= UART_PIN_RTS; 
				}
			}
			uart_pins(channel[c].fd, ttl_high, ttl_low);
			db_reply(msg, 0, NULL);
		}
	}else if(!strcmp(method, "set_dtr")){
		if(channel[c].fd >= 0){
			ttl_high = ttl_low = 0;
			if(p1 != NULL){
				if(atoi(p1)){
					ttl_high |= UART_PIN_DTR; 
				}else{
					ttl_low |= UART_PIN_DTR; 
				}
			}
			uart_pins(channel[c].fd, ttl_high, ttl_low);
			db_reply(msg, 0, NULL);
		}
	}else{
		db_reply(msg, -1, NULL);
	}
}

