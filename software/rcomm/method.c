#include "method.h"
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/sysmacros.h>

#ifdef USE_DBUS

#include <dbus/dbus.h>

int method_invoke(char *device, char *helper, char *helper_parameter, char *method, char *p1, char *p2, char *p3, char*p4, char *p5){
	struct stat s;
	int major, minor;
	char target[128];
	DBusError err;
	DBusConnection *conn;
	DBusMessage *msg;
	DBusMessageIter args;
	DBusPendingCall *pending;
/*	printf("METHOD: DEVICE: %s, HELPER: %s, HELPER PARAM: %s, METHOD: %s, P1: %s, P2: %s, P3: %s, P4: %s, P5: %s\n",
		device, helper, helper_parameter, method,
		p1, p2, p3, p4, p5
	);*/
	if(stat(device, &s) < 0){
		fprintf(stderr, "stat: %s\n", device);
		return(1);
	}
	if(!S_ISCHR(s.st_mode)){
		fprintf(stderr, "not char\n");
		return(1);
	}
	major = major(s.st_rdev);
	minor = minor(s.st_rdev);
//	printf("MAJOR: %d, MINOR: %d\n", major, minor);
	dbus_error_init(&err);
	conn = dbus_bus_get(DBUS_BUS_SESSION, &err);
	snprintf(target, 127, "com.rcomm.device_%d_%d", major, minor);
	target[127] = 0;
	msg = dbus_message_new_method_call(target, "/com/rcomm/device", "com.rcomm.device", method);
	dbus_message_iter_init_append(msg, &args);
	if(p1 != NULL)dbus_message_iter_append_basic(&args, DBUS_TYPE_STRING, &p1);
	if(p2 != NULL)dbus_message_iter_append_basic(&args, DBUS_TYPE_STRING, &p2);
	if(p3 != NULL)dbus_message_iter_append_basic(&args, DBUS_TYPE_STRING, &p3);
	if(p4 != NULL)dbus_message_iter_append_basic(&args, DBUS_TYPE_STRING, &p4);
	if(p5 != NULL)dbus_message_iter_append_basic(&args, DBUS_TYPE_STRING, &p5);
	dbus_connection_send_with_reply(conn, msg, &pending, -1);
	dbus_connection_flush(conn);
	dbus_pending_call_block(pending);
	msg = dbus_pending_call_steal_reply(pending);
	return(0);
}

#else
int method_invoke(char *device, char *helper, char *helper_parameter, char *method, char *p1, char *p2, char *p3, char*p4, char *p5){
	return(0);
}

#endif

