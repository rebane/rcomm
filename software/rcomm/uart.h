#ifndef _UART_H_
#define _UART_H_

#define UART_PARITY_NONE          0
#define UART_PARITY_EVEN          1
#define UART_PARITY_ODD           2
#define UART_PARITY_MARK          3
#define UART_PARITY_SPACE         4

#define UART_FLOW_NONE            0
#define UART_FLOW_RTSCTS          1

#define UART_PIN_RTS              0x01
#define UART_PIN_DTR              0x02

#define UART_RS485_ENABLED        0x01
#define UART_RS485_RTS_ON_SEND    0x02
#define UART_RS485_RTS_AFTER_SEND 0x04
#define UART_RS485_RX_DURING_TX   0x08
#define UART_RS485_TERMINATE_BUS  0x10

int uart_open(char *device);
int uart_close(int fd);
int uart_setup(int fd, unsigned long long int baudrate, int bits, int parity, int stop, int flow);
int uart_setup_standard(int fd, unsigned long long int baudrate, int bits, int parity, int stop, int flow);
int uart_setup_extended(int fd, unsigned long long int baudrate, int bits, int parity, int stop, int flow);
int uart_setup_rs485(int fd, int flags, unsigned int delay_rts_before_send, unsigned int delay_rts_after_send);
int uart_flush(int fd);
int uart_pins(int fd, int ttl_high, int ttl_low);
int uart_major(int fd);
int uart_minor(int fd);

#endif

