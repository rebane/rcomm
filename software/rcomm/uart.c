#include "uart.h"
#include <termios.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/sysmacros.h>
#include <linux/serial.h>

int uart_open(char *device){
	return(open(device, O_RDWR | O_NOCTTY | O_NONBLOCK));
}

int uart_close(int fd){
	return(close(fd));
}

int uart_setup(int fd, unsigned long long int baudrate, int bits, int parity, int stop, int flow){
	if(baudrate == 50){
		return(uart_setup_standard(fd, B50, bits, parity, stop, flow));
	}else if(baudrate == 75){
		return(uart_setup_standard(fd, B75, bits, parity, stop, flow));
	}else if(baudrate == 110){
		return(uart_setup_standard(fd, B110, bits, parity, stop, flow));
	}else if(baudrate == 134){
		return(uart_setup_standard(fd, B134, bits, parity, stop, flow));
	}else if(baudrate == 150){
		return(uart_setup_standard(fd, B150, bits, parity, stop, flow));
	}else if(baudrate == 200){
		return(uart_setup_standard(fd, B200, bits, parity, stop, flow));
	}else if(baudrate == 300){
		return(uart_setup_standard(fd, B300, bits, parity, stop, flow));
	}else if(baudrate == 600){
		return(uart_setup_standard(fd, B600, bits, parity, stop, flow));
	}else if(baudrate == 1200){
		return(uart_setup_standard(fd, B1200, bits, parity, stop, flow));
	}else if(baudrate == 1800){
		return(uart_setup_standard(fd, B1800, bits, parity, stop, flow));
	}else if(baudrate == 2400){
		return(uart_setup_standard(fd, B2400, bits, parity, stop, flow));
	}else if(baudrate == 4800){
		return(uart_setup_standard(fd, B4800, bits, parity, stop, flow));
	}else if(baudrate == 9600){
		return(uart_setup_standard(fd, B9600, bits, parity, stop, flow));
	}else if(baudrate == 19200){
		return(uart_setup_standard(fd, B19200, bits, parity, stop, flow));
	}else if(baudrate == 38400){
		return(uart_setup_standard(fd, B38400, bits, parity, stop, flow));
	}else if(baudrate == 57600){
		return(uart_setup_standard(fd, B57600, bits, parity, stop, flow));
	}else if(baudrate == 115200){
		return(uart_setup_standard(fd, B115200, bits, parity, stop, flow));
	}else if(baudrate == 230400){
		return(uart_setup_standard(fd, B230400, bits, parity, stop, flow));
	}else if(baudrate == 460800){
		return(uart_setup_standard(fd, B460800, bits, parity, stop, flow));
	}else if(baudrate == 500000){
		return(uart_setup_standard(fd, B500000, bits, parity, stop, flow));
	}else if(baudrate == 576000){
		return(uart_setup_standard(fd, B576000, bits, parity, stop, flow));
	}else if(baudrate == 921600){
		return(uart_setup_standard(fd, B921600, bits, parity, stop, flow));
	}else if(baudrate == 1000000){
		return(uart_setup_standard(fd, B1000000, bits, parity, stop, flow));
	}else if(baudrate == 1152000){
		return(uart_setup_standard(fd, B1152000, bits, parity, stop, flow));
	}else if(baudrate == 1500000){
		return(uart_setup_standard(fd, B1500000, bits, parity, stop, flow));
	}else if(baudrate == 2000000){
		return(uart_setup_standard(fd, B2000000, bits, parity, stop, flow));
	}else if(baudrate == 2500000){
		return(uart_setup_standard(fd, B2500000, bits, parity, stop, flow));
	}else if(baudrate == 3000000){
		return(uart_setup_standard(fd, B3000000, bits, parity, stop, flow));
	}else if(baudrate == 3500000){
		return(uart_setup_standard(fd, B3500000, bits, parity, stop, flow));
	}else if(baudrate == 4000000){
		return(uart_setup_standard(fd, B4000000, bits, parity, stop, flow));
	}
	return(uart_setup_extended(fd, baudrate, bits, parity, stop, flow));
}

int uart_setup_rs485(int fd, int flags, unsigned int delay_rts_before_send, unsigned int delay_rts_after_send){
	struct serial_rs485 s485;
	memset(&s485, 0, sizeof(s485));
	if(flags & UART_RS485_ENABLED)s485.flags |= SER_RS485_ENABLED;
	if(flags & UART_RS485_RTS_ON_SEND)s485.flags |= SER_RS485_RTS_ON_SEND;
	if(flags & UART_RS485_RTS_AFTER_SEND)s485.flags |= SER_RS485_RTS_AFTER_SEND;
	if(flags & UART_RS485_RX_DURING_TX)s485.flags |= SER_RS485_RX_DURING_TX;
#ifdef SER_RS485_TERMINATE_BUS
	if(flags & UART_RS485_TERMINATE_BUS)s485.flags |= SER_RS485_TERMINATE_BUS;
#endif
	s485.delay_rts_before_send = delay_rts_before_send;
	s485.delay_rts_after_send = delay_rts_after_send;
	return(ioctl(fd, TIOCSRS485, &s485));
}

int uart_flush(int fd){
	return(tcflush(fd, TCIOFLUSH));
}

int uart_pins(int fd, int ttl_high, int ttl_low){
	int ret, tio;
	ret = ioctl(fd, TIOCMGET, &tio);
	if(ret < 0)return(ret);
	if(ttl_high & UART_PIN_RTS){
		tio &= ~TIOCM_RTS;
	}
	if(ttl_high & UART_PIN_DTR){
		tio &= ~TIOCM_DTR;
	}
	if(ttl_low & UART_PIN_RTS){
		tio |= TIOCM_RTS;
	}
	if(ttl_low & UART_PIN_DTR){
		tio |= TIOCM_DTR;
	}
	return(ioctl(fd, TIOCMSET, &tio));
}

int uart_major(int fd){
	struct stat s;
	int ret;
	ret = fstat(fd, &s);
	if(ret < 0)return(ret);
	return(major(s.st_rdev));
}

int uart_minor(int fd){
	struct stat s;
	int ret;
	ret = fstat(fd, &s);
	if(ret < 0)return(ret);
	return(minor(s.st_rdev));
}

