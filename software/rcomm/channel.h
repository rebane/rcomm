#ifndef _CHANNEL_H_
#define _CHANNEL_H_

#define CHANNEL_LINE_BUFFER_LEN 1024

struct channel_struct{
	int fd;
	char *device;
	int use_helper;
	char *helper;
	char *helper_parameter;
	unsigned long int baudrate;
	int bits;
	int parity;
	int stop;
	int flow;
	int color;
	int receive_conversion;
	int hex_mode;
	int line_mode;
	int modem_rts;
	int modem_dtr;
	int rs485_flags;
	unsigned int rs485_delay_rts_before_send;
	unsigned int rs485_delay_rts_after_send;
	char line_buffer[CHANNEL_LINE_BUFFER_LEN];
	int line_loc;
	int major;
	int minor;
	int pause;
	int paused;
};

typedef struct channel_struct channel_t;

void channel_reset(channel_t *channel);
int channel_parse_baud(const char *baud, channel_t *channel);
int channel_parse_mode(const char *mode, channel_t *channel);
int channel_parse_bits(const char *bits);
int channel_parse_parity(const char *parity);
int channel_parse_stop(const char *stop);
int channel_parse_modem(const char *modem, channel_t *channel);

void channel_debug(int c, channel_t *channel);

#endif

