#include "fuser.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>

int fuser(const char *test, char *file, size_t size, pid_t *pids, int pids_count){
	pid_t pid;
	int out[2], err[2], status, l, c;
	char buffer[16];
	unsigned long long int p;
	if(pipe(out) < 0)return(-1);
	if(pipe(err) < 0){
		close(out[0]);
		close(out[1]);
		return(-2);
	}
	pid = fork();
	if(pid < 0){
		close(out[0]);
		close(out[1]);
		close(err[0]);
		close(err[1]);
		return(-3);
	}else if(pid == 0){
		dup2(out[1], 1);
		close(out[0]);
		close(out[1]);
		dup2(err[1], 2);
		close(err[0]);
		close(err[1]);
		execlp("fuser", "fuser", test, NULL);
		exit(1);
	}
	close(out[1]);
	close(err[1]);
	waitpid(pid, &status, 0);
	l = 0;
	c = 0;
	while(read(out[0], &buffer[l], 1) == 1){
		if((buffer[l] >= '0') && (buffer[l] <= '9')){
			if(l < 15)l++;
		}else{
			if(l){
				if(c < pids_count){
					buffer[l] = 0;
					p = strtoull(buffer, NULL, 10);
					pids[c++] = p;
				}
				l = 0;
			}
		}
	}
	if(l && (c < pids_count)){
		buffer[l] = 0;
		p = strtoull(buffer, NULL, 10);
		pids[c++] = p;
	}
	if(size){
		l = read(err[0], file, size);
		if(l > 0){
			file[l] = 0;
			while(l){
				if(file[l - 1] == ':'){
					file[l - 1] = 0;
					break;
				}
				if((file[l - 1] != ' ') && (file[l - 1] != '\t') && (file[l - 1] != '\r') && (file[l - 1] != '\n')){
					break;
				}
				file[--l] = 0;
			}
			if((l > 18) && (WEXITSTATUS(status) == 1) && !strncmp(file, "Specified filename ", 19)){
				strncpy(file, test, size);
				file[size - 1] = 0;
			}
		}else{
			file[0] = 0;
		}
	}
	return(c);
}

