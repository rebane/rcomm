#ifndef _DB_H_
#define _DB_H_

#include <sys/select.h>

void db_init();
void db_register(int major, int minor);
void db_unregister(int major, int minor);
int db_fd_set(int fd_max, fd_set *rfds, fd_set *wfds);
void db_fd_handle(fd_set *rfds, fd_set *wfds);
void db_reply(void *msg, int ret, char *p);

void db_callback(void *msg, int major, int minor, char *method, char *p1, char *p2, char *p3, char *p4, char *p5);

#endif

